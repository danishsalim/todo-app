let todoInput = document.querySelector("#input-todo");
let todoList = document.querySelector(".todo-list");
let footer = document.querySelector("footer");
let activeButton = document.querySelector("#active");
let completedButton = document.querySelector("#completed");
let allButton = document.querySelector("#all");
let clearCompletedButton = document.querySelector("#clear");
let selectAllCheckBox = document.querySelector(".toggle-all-checkbox");

todoInput.addEventListener("keydown", addTodo);
todoList.addEventListener("click", toggleCompleteTodo);
activeButton.addEventListener("click", showActiveTodo);
completedButton.addEventListener("click", showCompletedTodo);
allButton.addEventListener("click", showAllTodo);
clearCompletedButton.addEventListener("click", clearCompletedTodos);
selectAllCheckBox.addEventListener("change", toggleAllCheckBox);

function addTodo(event) {
  if (event.key == "Enter") {
    let newTodo = event.target.value.trim();
    if (newTodo != "") {
      let li = document.createElement("li");
      li.className = "list-item-group";
      let checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.className = "todo-check";
      li.appendChild(checkbox);
      let todoElement = document.createElement("input");
      todoElement.type = "text";
      todoElement.value = newTodo;
      todoElement.className = "todo-description";
      todoElement.setAttribute("readonly", "readonly");
      li.appendChild(todoElement);
      let deleteButton = document.createElement("button");
      deleteButton.className = "delete";
      deleteButton.appendChild(document.createTextNode("X"));
      li.appendChild(deleteButton);
      todoList.appendChild(li);
      todoInput.value = "";
      toggleFooterVisibilty();
      todoLeft();
      showSelectAllCheckbox();
    } else {
      return;
    }
  }
}

function toggleCompleteTodo(e) {
  if (e.target.classList.contains("todo-check")) {
    let li = e.target.parentElement;
    let todo = li.querySelector(".todo-description");
    todo.classList.toggle("completed");
  } else if (e.target.classList.contains("delete")) {
    deleteTodo(e);
  }
  todoLeft();
}

function deleteTodo(e) {
  let li = e.target.parentElement;
  todoList.removeChild(li);
  toggleFooterVisibilty();
  todoLeft();
  showSelectAllCheckbox();
}

function toggleFooterVisibilty() {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  if (allTodoLists.length == 0) {
    footer.classList.add("hide");
  } else {
    footer.classList.remove("hide");
  }
}

function showActiveTodo() {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  allTodoLists.forEach((list) => {
    let checkbox = list.firstElementChild;
    if (checkbox.checked) {
      list.classList.add("hide");
    } else {
      list.classList.remove("hide");
    }
  });
}

function showCompletedTodo() {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  allTodoLists.forEach((list) => {
    let checkbox = list.firstElementChild;
    if (!checkbox.checked) {
      list.classList.add("hide");
    } else {
      list.classList.remove("hide");
    }
  });
}

function showAllTodo() {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  allTodoLists.forEach((list) => {
    let checkbox = list.firstElementChild;
    list.classList.remove("hide");
  });
}

function clearCompletedTodos() {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  allTodoLists.forEach((list) => {
    let checkbox = list.firstElementChild;
    if (checkbox.checked) {
      todoList.removeChild(list);
      toggleFooterVisibilty();
      showSelectAllCheckbox();
    }
  });
  todoLeft();
}

function todoLeft() {
  let leftTodo = document.querySelector(".left-todo");
  let allTodoLists = document.querySelectorAll(".list-item-group");
  let count = 0;
  allTodoLists.forEach((list) => {
    let checkbox = list.firstElementChild;
    if (!checkbox.checked) {
      count++;
    }
  });
  if (count <= 1) {
    leftTodo.textContent = count + " item left!";
  } else {
    leftTodo.textContent = count + " items left!";
  }
}

function toggleAllCheckBox(e) {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  allTodoLists.forEach((list) => {
    let checkbox = list.firstElementChild;
    if (e.target.checked) {
      checkbox.checked = true;
    } else {
      checkbox.checked = false;
    }
  });
}

function showSelectAllCheckbox() {
  let allTodoLists = document.querySelectorAll(".list-item-group");
  if (allTodoLists.length == 0) {
    selectAllCheckBox.checked = false;
    selectAllCheckBox.classList.add("hide");
  } else {
    selectAllCheckBox.classList.remove("hide");
  }
}

toggleFooterVisibilty();
